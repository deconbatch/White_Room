* White Room.
One of the creative coding works by deconbatch.
[[https://www.deconbatch.com/2020/10/white-room-tried-to-express-rhythm-and.html]['White Room.' on Deconbatch's Creative Coding : Examples with Code.]]

[[./example01.png][An example image of one of the scene of this animation.]]

** Expressing the rhythm and density in a creative coding animation.
   - I tried to express the rhythm and density in a creative coding animation.
   - I've been reading Paul Klee's book. It's an exercise that will help me to understand this book well.
   - [[./density.png][At first, I drew a still image that expresses density.]]
   - And I added some deviation to it.
     - [[./density.deviation.01.png][Deviation example 01.]]
     - [[./density.deviation.02.png][Deviation example 02.]]
   - I drew still images that express rhythm also.
     - [[./rhythm.three.png][Rhythm example that expresses 'three'.]]
     - [[./rhythm.four.png][Rhythm example that expresses 'four'.]]
   - [[./ryhthm.density.png][Then, I combined these and added some deviations.]]
   - And I added express of 'weight' and some structure instead of a simple rectangle.
     - [[./object.light.png][Light]]
     - [[./object.heavy.png][Heavy]]
     - [[./structure.png][Some structure]]

** Yet another example images.
   - [[./example02.png][example image.]]
   - [[./example03.png][example image.]]

** Change log.
   - created : 2020/10/25
